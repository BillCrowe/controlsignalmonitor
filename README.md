![ControlSignalMonitorPreview](https://bitbucket.org/BillCrowe/controlsignalmonitor/raw/4b7be7638f0d58115e2ebd07288226477aa9c728/ControlSignalMonitorV1.1_Screenshot.PNG)

Help File

Control Signal Monitor v1.1

General Notes

- The Control Signal Monitor component is intended for use as a testing and troubleshooting tool to display current control signal values. The Trace Log window displays time stamped entries when a control signal value changes.

Controls

- Component Name ( String )- Selects the Named Component controls that will be available in the Component Control combo box
- Component Control ( String )- Selects the Named Component’s control that will be displayed and logged 
- Control Values ( String )- Displays the current control values of the selected component’s control
- Enable ( Boolean )- Enables/disabled the display and logging of a Named Component’s control
- Trace Log Clear ( Trigger )- Clears the entries in the Trace Log Window
- Trace Log Window ( String )- Displays time stamped entries for control value changes
    
Application Notes

- To monitor a component’s control value changes, the components must be a Named Component with a unique component name.

Developer: Bill Crowe

Company: QSC

Version 1.1

- Released Date: 2021.07.29
- Tested Firmware Version: 9.1.2
- Tested Hardware: Core 110f
- Notes: Corrected "Max execution limits exceeded" error for designs with a large number of Named Components

Version 1.0

- Released Date: 2021.01.05
- Tested Firmware Version: 8.4
- Tested Hardware: Core 110f
- Notes: Initial release